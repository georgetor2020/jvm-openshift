FROM xDOCKER_REGISTRYx/xDOCKER_IMAGEx:xDOCKER_TAGx
ARG GRADLE_BIN=build/libs/xGRADLE_FILEx
ARG ENV_CONTEXT=xGRADLE_NAMEx
RUN test -n $GRADLE_BIN || (echo "Build-args BIN_NAME is required but missing." && false)

RUN apk add tzdata \
  && cp /usr/share/zoneinfo/America/Toronto /etc/localtime \
  && mkdir -p /app/config 

ENV BASE_JAVA_OPTS -D"java.security.egd=file:/dev/./urandom" \
  -D"user.timezone=America/Toronto"

COPY $GRADLE_BIN /app/app.jar

RUN echo '#!/bin/sh' > /entrypoint.sh \
	&& echo 'set -x' >> /entrypoint.sh \
	&& echo 'sh -c "cd /app; exec java $BASE_JAVA_OPTS -DpublishAuthPwd=$JAVA_OPTS -Dresmgr=$JAVA_OPTS2 -jar app.jar --server.servlet.context-path=/$ENV_CONTEXT"'  >> /entrypoint.sh \
	&& chmod a+x /entrypoint.sh
	 
ENTRYPOINT /entrypoint.sh
